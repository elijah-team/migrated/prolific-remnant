package tripleo.elijah.comp;

public interface CSS2_Signal {
    void trigger(Compilation compilation, Object payload);
}
