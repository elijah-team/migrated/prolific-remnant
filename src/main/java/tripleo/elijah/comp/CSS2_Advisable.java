package tripleo.elijah.comp;

public interface CSS2_Advisable {
	void adviseObject(Object aPayload);
}
